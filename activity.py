from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod

    def eat(self, food):
        pass
    
    def make_sound(self):
        pass

class Dog(Animal):

    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    def set_name(self, name):
        self._name = name
        
    def set_breed(self, name):
        self._breed = breed

    def set_age(self, name):
        self._age = age

    def get_name(self):
        print(f'The Dog is {self._name}')

    def get_breed(self):
        print(f'The Dog is {self._breed}')
    
    def get_age(self):
        print(f'The Dog is {self._age}')

    def eat(self, food):
        print(f"{self._name}, the dog, is eating the {food} ")

    def make_sound(self):
        print('woof woof!')

    def call(self):
        print(f'Come here {self._name}')

class Cat(Animal):

    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    def set_name(self, name):
        self._name = name
        
    def set_breed(self, name):
        self._breed = breed

    def set_age(self, name):
        self._age = age

    def get_name(self):
        print(f'The Dog is {self._name}')

    def get_breed(self):
        print(f'The Dog is {self._breed}')
    
    def get_age(self):
        print(f'The Cat is {self._age}')

    def eat(self, food):
        print(f"{self._name}, the cat, is eating the {food} ")

    def make_sound(self):
        print('meow meow!')

    def call(self):
        print(f'{self._name}, Come on')

d1 = Dog('Jamian', 'Rotweiler', 5)
d1.eat('Jade')
d1.make_sound()
d1.call()

c1 = Cat('Rey', 'Garfield', 4)
c1.eat('cat nips')
c1.make_sound()
c1.call()

